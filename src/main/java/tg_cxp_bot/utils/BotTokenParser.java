package tg_cxp_bot.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public final class BotTokenParser {

	final String configFileName = "config.properties";
	
	private String readPropFile() throws Exception {
		
		Properties props = new Properties();
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(configFileName);

		if (inputStream != null) {
			try {
				props.load(inputStream);
				String token = props.getProperty("BOT_TOKEN");
				return token;
			} catch (IOException e) {
				System.err.println(e.getMessage());
				System.exit(1);
			}
		}
		throw new Exception("Token can't be reached");
	}
	
	public String getToken() throws Exception {
		String token = readPropFile();
		return token;
	}
}
