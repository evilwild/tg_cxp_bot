package tg_cxp_bot.scrapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import tg_cxp_bot.utils.StatusComparator;

public final class Scrapper {

	final String HOMEPAGE_URL = "https://cavexp.com/feed";
	final String EL_STATUSBOX = "servers_status_box";
	final String EL_SERVERS = "server_status_entry";
	final String EL_SERVER_TITLE = "server_title";
	final String EL_SERVER_STATUS = "server_desc";

	private Elements getServerFrame() {
		try {
			return Jsoup.connect(HOMEPAGE_URL).get().getElementById(EL_STATUSBOX).getElementsByClass(EL_SERVERS);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return null;
	}

	private HashMap<String, Integer> parseServerMap(Elements servers) {
		HashMap<String, Integer> serversMap = new HashMap<String, Integer>();
		for (Element server : servers) {
			String title = server.getElementsByClass(EL_SERVER_TITLE).text();
			String statusStr = server.getElementsByClass(EL_SERVER_STATUS).text();
			int status = translateStatus(statusStr);
			serversMap.put(title, status);
		}
		return serversMap;
	}

	public String getStringStatuses() throws IOException {
		StringBuilder statuses = new StringBuilder();
		HashMap<String, Integer> servers = parseServerMap(getServerFrame());

		servers.forEach((title, status) -> {
			statuses.append(title);
			statuses.append(" ");
			statuses.append(status);
			statuses.append("\n");
		});
		
		statuses.setLength(statuses.length()-1);
		
		return statuses.toString();
	}

	public String refreshStatuses(String messageToEdit) throws IOException {
		StringBuilder updatedStatuses = new StringBuilder();
		HashMap<String, Integer> servers = parseServerMap(getServerFrame());
		HashMap<String, Integer> oldServers = parseOldServersMap(messageToEdit);

		Iterator<Entry<String, Integer>> it1 = servers.entrySet().iterator();
		Iterator<Entry<String, Integer>> it2 = oldServers.entrySet().iterator();
		
		while(it1.hasNext() && it2.hasNext()) {
			Map.Entry<String, Integer> serverInfo = it1.next();
			Map.Entry<String, Integer> oldServerInfo = it2.next();
			String title = serverInfo.getKey();
			int newStatus = serverInfo.getValue();
			String difference = StatusComparator.compareStatus(serverInfo.getValue(), oldServerInfo.getValue());
			
			updatedStatuses.append(title);
			updatedStatuses.append(" ");
			updatedStatuses.append(newStatus);
			if (difference != null) {
				updatedStatuses.append(" ");
				updatedStatuses.append(difference);
			}
			updatedStatuses.append("\n");
			
			it1.remove();
			it2.remove();
		}
		
		return updatedStatuses.toString();
	}
	
	public String clearDifference(String status) {
		return status.replaceAll(" \\(*\\)", "");
	}

	int translateStatus(String status_str) {
		if (status_str == "Сервер перезагружается") {
			return -1;			
		} else if (status_str.contains("Игроков онлайн: ")) {
			status_str = status_str.replaceAll("[а-яА-Я: ]", "");	
			try {
				int result = Integer.parseInt(status_str);
				return result;
			} catch (NumberFormatException ex) {
				return -1;
			}
		}
		return -1;
	}

	private HashMap<String, Integer> parseOldServersMap(String message) {
		HashMap<String, Integer> servers = new HashMap<String, Integer>();
		message = clearDifference(message);
		String[] lines = message.split("\n");
		for (String line : lines) {
			String[] line_component = line.split(" ");
			servers.put(line_component[0], Integer.parseInt(line_component[1]));
		}
		return servers;
	}
}
