package tg_cxp_bot;

import java.io.IOException;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.model.CallbackQuery;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.model.request.InlineKeyboardButton;
import com.pengrad.telegrambot.model.request.InlineKeyboardMarkup;
import com.pengrad.telegrambot.request.AnswerCallbackQuery;
import com.pengrad.telegrambot.request.EditMessageText;
import com.pengrad.telegrambot.request.SendMessage;

import tg_cxp_bot.scrapper.Scrapper;
import tg_cxp_bot.utils.BotTokenParser;

public class Bot {

	TelegramBot bot;
	Scrapper scrapper;
	InlineKeyboardMarkup inlineKeyboard = new InlineKeyboardMarkup(
			new InlineKeyboardButton[] { new InlineKeyboardButton("Refresh").callbackData("refresh"), });

	public Bot() {
		scrapper = new Scrapper();
		String token = null;
		try {
			token = new BotTokenParser().getToken();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		bot = new TelegramBot(token);
	}

	public void run() {
		bot.setUpdatesListener(updates -> {
			for (Update update : updates) {
				CallbackQuery callbackQuery = update.callbackQuery();
				if (callbackQuery != null) {
					EditMessageText request = callbackAnswer(callbackQuery);
					bot.execute(request);
					continue;
				}
				Message received_msg = update.message();
				SendMessage request = answerMessage(received_msg);
				bot.execute(request);
			}
			return UpdatesListener.CONFIRMED_UPDATES_ALL;
		}, e -> {
			if (e.response() != null) {
				e.response().errorCode();
				e.response().description();
			} else {
				e.printStackTrace();
			}
		});
	}

	private SendMessage answerMessage(Message msg) {
		String text = msg.text();
		long chatId = msg.chat().id();
		if (text == null) {
			return new SendMessage(chatId, "Error");
		}
		String response_msg;
		
		switch (text) {
		case "/status":
			try {
				response_msg = scrapper.getStringStatuses();
				return new SendMessage(chatId, response_msg).replyMarkup(inlineKeyboard);
			} catch (IOException JsoupEx) {
				return new SendMessage(chatId, "Error").replyMarkup(inlineKeyboard);
			}
		default:
			response_msg = "Incorrect command, type /status";
			return new SendMessage(chatId, response_msg);
		}
	}

	private EditMessageText callbackAnswer(CallbackQuery cb) {
		AnswerCallbackQuery answerCb = new AnswerCallbackQuery(cb.id());
		Message cbMessage = cb.message();
		int cbMessageId = cbMessage.messageId();
		long cbChatId = cbMessage.chat().id();
		String cbMessageText = cbMessage.text();
		try {
			 String response_msg = scrapper.refreshStatuses(cbMessageText);
			 bot.execute(answerCb);
			 return new EditMessageText(cbChatId, cbMessageId, response_msg).replyMarkup(inlineKeyboard);
		} catch (IOException e){
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return null;
	}
}
